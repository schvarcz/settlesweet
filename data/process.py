import pandas
from argparse import ArgumentParser

"""
This script serves as a tool to filter the input that.
My assumptions when I run it to generate the file used in the webpage was:
    "It is very unlikely that two ads, located in the same region will have the exactly same price and surface/rooms."

I am aware that this is not a strong assumption. But better than that, just if we evaluate the description field (probably with a NLP strategy).
Unfortunately, I don't have time to do it. However, I could give TF-IDF a shot. And this is why we have another python file here.
"""

def get_arguments():
    parser = ArgumentParser()
    parser.add_argument(
        "--price-min",
        type=float,
        default=0,
        help="Minimal price. Default: 0"
    )

    parser.add_argument(
        "--price-max",
        type=float,
        default=float("inf"),
        help="Maximal price. Default: Infinty."
    )

    parser.add_argument(
        "--surface-min",
        type=float,
        default=0,
        help="Minimal surface. Default: 0"
    )

    parser.add_argument(
        "--surface-max",
        type=float,
        default=float("inf"),
        help="Maximal surface. Default: Infinty."
    )

    parser.add_argument(
        "--hard-columns",
        type=str,
        nargs="+",
        default=["rooms", "superficy", "city", "zip_code", "price"],
        help='List of columns that should not accept any duplicated entry. It is an "and" list. Default: ["rooms", "superficy", "city", "zip_code", "price"].'
    )

    parser.add_argument(
        "-i", "--input",
        type=str,
        default='input.csv',
        help="Input file to be cleaned. Default: input.csv"
    )

    parser.add_argument(
        "-o", "--output",
        type=str,
        default='filtered.csv',
        help="Output cleaned file. Default: filtered.csv"
    )

    parser.add_argument(
        "--output-removed",
        type=str,
        default='removed.csv',
        help="Output file with removed entries. Default: removed.csv"
    )

    parser.add_argument(
        "-d", "--debug",
        # dest='debug_output_removed',
        action='store_true',
        help="A flag to indicate if the 'removed output' should also contain the first good entry. Just to debug if the selection was good or not."
    )

    return parser.parse_args()

args = get_arguments()

original_dataset = pandas.read_csv(args.input)

cur_selection = ~original_dataset.duplicated(args.hard_columns)
cur_selection &= original_dataset["price"].between(args.price_min, args.price_max)
cur_selection &= original_dataset["superficy"].between(args.surface_min, args.surface_max)

original_dataset[cur_selection].to_csv(args.output)
if not args.debug:
    original_dataset[~cur_selection].to_csv(args.output_removed)
else:
    cur_selection = ~original_dataset.duplicated(args.hard_columns, keep=False)
    cur_selection &= original_dataset["price"].between(args.price_min, args.price_max)
    cur_selection &= original_dataset["superficy"].between(args.surface_min, args.surface_max)
    original_dataset[~cur_selection].to_csv(args.output_removed)
