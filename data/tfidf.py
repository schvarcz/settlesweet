import pandas
import numpy as np
from matplotlib import pyplot as plt
from argparse import ArgumentParser

"""
In this script we compute a TF-IDF measurment for the description field to highlight.
Two high similar desc fields should highlight a high probability of double posting.
"""

def get_arguments():
    parser = ArgumentParser()
    parser.add_argument(
        "--tfidf-threshold",
        type=float,
        default=5,
        help="TF-IDF threshold. Default: 5"
    )

    parser.add_argument(
        "--bins",
        type=float,
        default=100,
        help="Nro bins on the histogram. Default: 100"
    )

    parser.add_argument(
        "-i", "--input",
        type=str,
        default='input.csv',
        help="Input file to be cleaned. Default: input.csv"
    )

    parser.add_argument(
        "-o", "--output",
        type=str,
        default='filtered.csv',
        help="Output cleaned file. Default: filtered.csv"
    )

    return parser.parse_args()


args = get_arguments()

original_dataset = pandas.read_csv(args.input)
descs = original_dataset["description"].tolist()

pontuation = open("pontuation","r").read().splitlines()
stopwords = open("stopwords","r").read().splitlines()

# Cleaning up the data
for i in range(len(descs)):
    for p in pontuation:
        descs[i] = descs[i].replace(p, " ")

    for w in stopwords:
        descs[i] = descs[i].lower().replace(" {0} ".format(w), "")

    descs[i] = " ".join(descs[i].split())

# Calculating the TF and IDF
allwords = " ".join(descs).split()
allwords_dict = {}
for w in allwords:
    allwords_dict[w] = 0

unique_words, doc_idf = allwords_dict.keys(), allwords_dict.values()

doc_tf = [[] for i in range(len(descs))]

#This part take some time to compute.
for i in range(len(descs)):
    terms = descs[i].split()
    for j, w in enumerate(unique_words):
        doc_tf[i].append(terms.count(w))
        if w in terms:
            doc_idf[j] += 1


doc_idf = np.asarray(doc_idf).astype(np.float)
doc_tf = np.asarray(doc_tf).astype(np.float)

doc_tf_w = np.multiply(doc_tf, np.log(len(descs)/doc_idf)) # tf-idf computation

correlation_matrix = doc_tf_w.dot(doc_tf_w.T)

correlation_matrix[correlation_matrix==0] = 0.1 # Just to avoid log(0)
ml = np.log(correlation_matrix)
mlc = ml.copy() # Just for debugging on ipython.

# Remove negative logs (some of the input were close to 0)
mlc[mlc<0] = 0

# Plot the histogram
y, x = np.histogram(mlc, bins=args.bins)
plt.plot(x[:args.bins],y)
plt.title("Correlation Histogram")


# Uncomment these lines to remove the diagonal line in the correlation matrix
# for i in range(m.shape[0]):
#     mlc[i,i] = 0

# Show the filtered correlation matrix.
mlc[mlc < args.tfidf_threshold] = 0
plt.figure()
plt.imshow(mlc)
plt.colorbar()
plt.title("Correlation Matrix")
plt.show()


# Take the indeces to filter the file, but I have to wrap it up.
d1, d2 = np.nonzero(mlc)

to_remove = [ False for i in range(len(descs)) ]
for i in range(len(d1)):
    if d1[i] != d2[i]:
        to_remove[d1[i]] = True
        to_remove[d2[i]] = True
