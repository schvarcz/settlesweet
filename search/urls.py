from django.urls import path

from . import views as v

urlpatterns = [
    path('', v.index, name="index"),
    path('search', v.search, name="search"),
    path('details', v.details, name="details")
]
