from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
import json, pandas, os
from .forms import SearchForm, DetailsForm
from random import shuffle
import numpy as np

# List of possible pictures to the add.
img_example = ["example.jpg", "example2.jpg", "example3.jpg", "example4.jpg", "example5.jpg"]

# Index page. Access point
def index(request):
    return render(request, "index.html")


# REST api to list all apts
def search(request):
    # I could have done it with django admin/models, but since I am still treating the data, I will be using pandas for while.
    cur_data = pandas.read_csv(os.path.dirname(__file__)+"/data/filtered.csv")

    searchForm = SearchForm(request.GET)

    def safe_cast(val, default=None):
        try:
            return float(val)
        except (ValueError, TypeError):
            return default

    city = searchForm["city"].value() if not searchForm["city"].value() is None else ""
    surface_min, surface_max = safe_cast(searchForm["surface_min"].value(), 0.), safe_cast(searchForm["surface_max"].value(), float("inf"))
    price_min, price_max = safe_cast(searchForm["price_min"].value(), 0.), safe_cast(searchForm["price_max"].value(), float("inf"))
    one_room = searchForm["one_room"].value()
    two_room = searchForm["two_room"].value()
    three_room = searchForm["three_room"].value()

    rooms = []
    if one_room:
        rooms.append(1)
    if two_room:
        rooms.append(2)
    if three_room:
        rooms.append(3)

    if city != "":
        cur_data = cur_data[cur_data['city'] == city]

    if not rooms == []:
        cur_data = cur_data[cur_data['rooms'].isin(rooms)]

    cur_data = cur_data[cur_data['superficy'].between(surface_min, surface_max)]
    cur_data = cur_data[cur_data['price'].between(price_min, price_max)]

    cur_data['pic'] = np.random.choice(img_example, cur_data.shape[0])

    totalRecords = len(cur_data)
    list = cur_data.head(200) # Just for debugging. I will not have time to make an "infinity scroll"

    data = {
      "totalRecords": totalRecords,
      "apts" : json.loads(list.to_json(orient="records")),
    }
    return JsonResponse(data)


# REST api to get the details of an offer
def details(request):
    # I could have done it with django admin/models, but since I am still treating the data, I will be using pandas for while.
    cur_data = pandas.read_csv(os.path.dirname(__file__)+"/data/filtered.csv")
    cur_data = cur_data.set_index("listingId")

    detailForm = DetailsForm(request.GET)

    if detailForm.is_valid():
        listing_id = int(detailForm["listing_id"].value())
        json_apt = json.loads(cur_data.loc[listing_id].to_json())
    else:
        json_apt = json.loads(cur_data.iloc[0].to_json())

    shuffle(img_example)
    json_apt["pics"] = img_example

    return JsonResponse({ "apt" : json_apt })
