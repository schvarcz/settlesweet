from django import forms

class SearchForm(forms.Form):
    city = forms.CharField(initial="")
    surface_min = forms.DecimalField(initial=0)
    surface_max = forms.DecimalField(initial=float("inf"))
    price_min = forms.DecimalField(initial=0)
    price_max = forms.DecimalField(initial=float("inf"))
    one_room = forms.BooleanField(initial=False)
    two_room = forms.BooleanField(initial=False)
    three_room = forms.BooleanField(initial=False)


class DetailsForm(forms.Form):
    listing_id = forms.IntegerField(required=True)
